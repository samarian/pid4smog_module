import setuptools
with open("README.md", "r") as fh:
        long_description = fh.read()

setuptools.setup(
         name='pid4smog',
         version='0.0.0',
         scripts=[] ,
         author="Saverio Mariani",
         author_email="mariani@fi.infn.it",
         description="Package to describe a bidimensional classifier marginal pdf on a set of relevant features employing a Maximum Likelihood fit with a Gaussian Mixture Model whose parameters are predicted by MultiLayer Perceptron Neural Networks.",
         long_description=long_description,
         long_description_content_type="text/markdown",
         url="https://gitlab.cern.ch/samarian/pid4smog_module",
         packages=setuptools.find_packages(),
         classifiers=[
                      "Programming Language :: Python :: 2.7",
                      "Programming Language :: Python :: 3.4",
                      "Programming Language :: Python :: 3.5",
                      "License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)",
                      "Operating System :: OS Independent",
                  ],
        install_requires=[ "root_numpy>=4.8.0", "root_pandas>=0.6.1", "root_report>=0.1.7", "scikit_learn>=0.21.2",
                           #"tensorflow-gpu>=1.13.1, <2.0.0", "tensorflow>=1.13.1, <2.0.0", 
                           "pandas>=0.24.2", "numpy>=1.16.3, <1.19.0", "tqdm",
                           "tensorflow-probability==0.8.0"
        ]
     )
