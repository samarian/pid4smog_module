#########################################################################
#########################################################################
# Project: pid4smog                                                     #
#    File: $Id: utils/Histo_2DComparison                                #
# @authors = Saverio Mariani mariani@fi.infn.it     1,2                 #
#          = Lucio Anderlini anderlinil@fi.infn.it  2                   #
#                                                                       #
#          1 Universita' degli studi di Firenze                         #
#          2 INFN, Sezione di Firenze                                   #
#  Copyright (c) 2019 - CERN on behalf of the LHCb Collaboration        #
#                                                                       #
#  Redistribution and use in source and binary forms,                   #
#  with or without modification, are permitted according to the terms   #
#  listed in LICENSE                                                    #
#                                                                       #
#########################################################################
#########################################################################
from array import array
import ROOT
import numpy as np

class Histo_2DComparison:
    """
    Hist_2DComparison
    =========

    Class to compare bidimensional histogrammed distributions. The decoupling wrt other classes allows to quantify
    the quality of both the model validation and the application to pHe datas and to employ different statistical
    indicators.
    """

    def __init__( self, stat_test = "KS_Distance"  ):
        """
        Histo_2DComparison.__init__ : class initialization
        stat_test = <str> statistical test identifier (KS_Distance, p_Value [Energy test and BDT not yet implemented])
        """
        self.histos = {}
        self.stat_test = stat_test

    def SetBins(self, bins, report, hist_prefix = "KS"):
        """
        Histo_2DComparison.SetBins : histogram initialization
        bins = <dict< str, list<float> > > dictionary with list of bin limits for each watcher variable
        hist_prefix = <str> histogram name common prefix
        """
        for ifeat, ibins in bins.items():
            for jfeat, jbins in bins.items():
                if list(bins.keys()).index(jfeat) <= list(bins.keys()).index(ifeat) : continue
                tag = ifeat + "_" + jfeat
                self.histos[tag] = report.make_hist( str.join("_", ["Histo", hist_prefix, tag]),
                                                     report.ax_helper([ifeat, jfeat]), np.zeros( shape = (1, 2) ),
                                                     nbins = [ibins, jbins], fill_mode = "empty" )

    def Compare( self, histo_tag, histograms, mins ):
        """
        Histo_2DComparison.Compare : Statistical 2D distribution comparison evaluation and histogram filling
        histo_tag = <str> identifier of the histogram to be filled
        histograms = <list<TH2> > 2d histograms to be compared
        mins = <list <float>> x and y bin minima to find bin to be filled
        """
        if self.stat_test == "KS_Distance":
            ivalue = histograms[0].KolmogorovTest( histograms[1], "M")

        elif self.stat_test == "pValue":
            ivalue = h_pred.KolmogorovTest(h_data)

        self.histos[histo_tag].SetBinContent( self.histos[histo_tag].GetXaxis().FindBin(mins[0]),
                                              self.histos[histo_tag].GetYaxis().FindBin(mins[1]), ivalue)
        return ivalue

    def DrawHist( self, report, lat = None, options = "width = 90%", text_format = "2.2f", log = False ):
        """
        Histo_2DComparison.DrawHist : drawing of the
        report = <HistReport instance> report to save histogram on
        lat = <str/list<str>> TLatex messages to draw on canvas
        text_format = <str> numbers formatting in 2D plots
        """
        report.set_textformat( text_format )

        for itag, ih in self.histos.items():
            ih.SetMarkerColor(ROOT.kBlack)
            report.draw_hist( ih.GetName() , ih, goptions = "COLZ TEXT", options = options, lat = lat, log = log )
