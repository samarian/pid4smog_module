#########################################################################
#########################################################################
# Project: pid4smog                                                     #
#    File: $Id: Training.py                                             #
# @authors = Saverio Mariani mariani@fi.infn.it     1,2                 #
#          = Lucio Anderlini anderlinil@fi.infn.it  2                   #
#                                                                       #
#          1 Universita' degli studi di Firenze                         #
#          2 INFN, Sezione di Firenze                                   #
#  Copyright (c) 2019 - CERN on behalf of the LHCb Collaboration        #
#                                                                       #
#  Redistribution and use in source and binary forms,                   #
#  with or without modification, are permitted according to the terms   #
#  listed in LICENSE                                                    #
#                                                                       #
#########################################################################
#########################################################################
import pickle
from array import array

import ROOT
import numpy as np
import pandas as pd
import tensorflow as tf

import sys

class Overtraining:
    """
    Overtraining
    ============
    Class to perform a minimal check that overtraining does not apply. Differently from a classification or a
    regression task, in this particular application overtraining could only apply as a fast variation of the
    Gaussian parameters to adapt to the dataset. Scanning the Gaussian parameters as a function of the featuress,
    there is no overtraining if smooth profiles are found.
    """

    def __init__(self, dataset, parameters, vars_to_validate = None):
        """
        ###Class members definition

        arguments:
           dataset    : (pandas DataFrame) calibration data
           parameters : (dict <string, <> >) with keys :
                            trainDir   : (str)  directory where to load the training model from
                            nBins      : (int)  number of features bins
                            nSamples   : (int)  number of uniformly generated entries for validation
                            nGaussian  : (int)  number of Gaussian in the model
                            G_pars     : (list<string>) Gaussian parameters to scan (dflt [mean, width, weights] )
                            vars_to_validate : (list<string>) : list of features to check the evolution wrt (dflt all features)
        """
        self.dataset  = dataset
        self.vars_to_validate = parameters['vars_to_validate']

        #parameters
        self.trainDir = parameters["trainDir"]
        self.nBins    = parameters["nBins"]
        self.nSamples = parameters["nSamples"]
        self.nGaussian= parameters["nGaussians"] #Can this be accessed from the trainedModel?
        if "G_pars" in parameters:
            self.G_pars = parameters["G_pars"]
        else: self.G_pars = [ "mean", "width", "weights" ]
        ##################################################################################

    def pars_evolution( self, report):
        """
        ###Scan of the Gaussian parameters as a function of the features variables

        arguments:
             report : (HistReport istance) report where to save histograms
        """
        histos = dict()

        colors = [ ROOT.kRed, ROOT.kOrange, ROOT.kGreen, ROOT.kCyan, ROOT.kMagenta, ROOT.kBlue, ROOT.kBlack,
                   ROOT.kGray, ROOT.kViolet, ROOT.kSpring, ROOT.kPink, ROOT.kTeal, ROOT.kAzure, ROOT.kYellow]
        colors += [ i - 3 for i in colors ] + [ i + 5 for i in colors ] + [ i - 1 for i in colors ]

        print( self.trainDir )
        with tf.Session() as ss:
            scalers = pickle.load ( open ( self.trainDir + "/scalers.pkl" , 'rb' ) )
            target    = [ivar.replace("targ_", "") for ivar in scalers if "targ_" in ivar]
            features  = [ivar.replace("feat_", "") for ivar in scalers if "feat_" in ivar]

            if self.vars_to_validate is None : self.vars_to_validate = features

            tf.saved_model.load (ss, ['serve'], self.trainDir)
            for ifeat in self.vars_to_validate:
                limits = list(pd.qcut( self.dataset[ifeat].values, q = self.nBins, labels = False, retbins = True)[1])

                for itarg in target:
                    for ipar in self.G_pars:
                        h_tag = str.join("_", [ipar, ifeat, itarg])
                        histos [h_tag] = [
                            ROOT.TH1D( "H_" + h_tag +  str(i),
                                       "; #scale[1.2]{#bf{%s}}; #scale[1.2]{#bf{Gaussian distributions %s}}"%(
                                           report.ax_helper(ifeat), ipar),
                                       self.nBins -1, array('d', limits) ) for i in range(self.nGaussian) ]

                print("Studying now parameter evolutions with " + ifeat)
                for imin, imax in zip(limits[:-1], limits[1:]):

                    featuresspace, scld_featuresspace = [], []
                    for jfeat in features:
                        if jfeat == ifeat:
                            varSer = np.full(shape = (self.nSamples), fill_value = (imax+imin)/2. )
                            featuresspace.append( varSer )
                            scld_featuresspace.append(scalers["feat_" + jfeat].transform(
                                featuresspace[-1].reshape(-1, 1))[:,0])
                        else :  scld_featuresspace.append( np.zeros(shape = (self.nSamples)) )

                    scld_featuresspace = np.array ( scld_featuresspace ) . T
                    av = dict()
                    for ipar in self.G_pars:
                        evaluated = ss.run ( ipar + ":0" , feed_dict = { "input_c:0" : scld_featuresspace } )
                        av[ipar] = np.mean( evaluated, axis = 0)

                        for i, itarg in enumerate(target):
                            for icol in range(self.nGaussian):
                                fill_value = av[ipar][icol][i] if len(av[ipar].shape) > 1 else av[ipar][icol]
                                histos[ipar + "_" + ifeat + "_" + itarg][icol] . SetBinContent(
                                    histos[ipar + "_" + ifeat + "_" + itarg][icol].FindBin(imin), fill_value)

            for ifeat in self.vars_to_validate:
                report << "Dependence on " << ifeat << report.br
                for ipar in self.G_pars:
                    for indx, itarg in enumerate(target):
                        h_tag = ipar + "_" + ifeat + "_" + itarg
                        list_hist = histos[ h_tag ]
                        for i in range(self.nGaussian):
                            list_hist[i].SetLineColor( colors[i] )

                            hist_content = av[ipar][:,indx] if len(av[ipar].shape) > 1 else av[ipar]
                            list_hist[i].SetMinimum( 0.5*np.min( hist_content ) )
                            list_hist[i].SetMaximum( 1.5*np.max( hist_content ))

                        report.draw_hist ( h_tag, list_hist, outputCanvas = False, goptions = "L" )
                        report.draw_latex( h_tag + "__", outputCanvas = False, lat = report.lat_sample() )
                        report.draw_latex( h_tag + "_", x = 0.75, lat = report.ax_helper(itarg))
