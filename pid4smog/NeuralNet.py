#########################################################################
#########################################################################
# Project: pid4smog                                                     #
#    File: $Id: NeuralNet.py                                            #   
# @authors = Saverio Mariani mariani@fi.infn.it     1,2                 # 
#          = Lucio Anderlini anderlinil@fi.infn.it  2                   #  
#                                                                       # 
#          1 Universita' degli studi di Firenze                         # 
#          2 INFN, Sezione di Firenze                                   #      
#  Copyright (c) 2019 - CERN on behalf of the LHCb Collaboration        # 
#                                                                       #       
#  Redistribution and use in source and binary forms,                   #      
#  with or without modification, are permitted according to the terms   #      
#  listed in LICENSE                                                    #  
#                                                                       #
#########################################################################
#########################################################################
import numpy as np
import tensorflow as tf

class NeuralNet:
    """
    NeuralNet
    =========

    Neural network implmentation to study PID dependencies on feature variables.
    """
    
    def __init__( self, nLayers = 2, nNodes = 32, Activation = 'tanh' ):
        """
        ### Class members definition
        
        arguments:
           nLayers      : (int) Number of hidden layers  
           nNodes       : (int) Number of nodes in the NN hidden layers
           Activation   : (string) Activation function for the hidden NN layers
        """

        self.nLayers = nLayers
        self.nNodes  = nNodes
        self.Activation = Activation
    ################################################################################

        
    def NN_model(self, out_shape):
        """
        #### Definition of the neural network 

        arguments:
            out_shape: (tuple<int>) outputNode shape
        returns:
            model: keras NN model
        """

        model = tf.keras.models.Sequential()
        for i in range( self.nLayers ):
            model . add ( tf.keras.layers.Dense ( self.nNodes, activation = self.Activation ) )

        model . add ( tf.keras.layers.Dense ( np.prod(np.array(out_shape)),
                                              kernel_initializer ='zeros',
                                              bias_initializer   ='zeros'))
        model . add ( tf.keras.layers.Reshape (( out_shape)))

        return model
    ################################################################################
