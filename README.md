PID4SMOG
==============

Package to describe a bidimensional classifier marginal pdf on a set of relevant features employing a Maximum Likelihood fit with a Gaussian Mixture Model whose parameters are predicted by MultiLayer Perceptron Neural Networks. Motivations of the project and a full proof-of-principle can be found [in the paper](https://arxiv.org/pdf/2110.10259.pdf). The LHCb-specific code for such a proof-of-principle can be found to [this LHCb-restricted-only link](https://gitlab.cern.ch/samarian/pid4smog).


Config_example and tutorial.ipynb contain a minimal working example on terminal or interactive mode, respectively. Graphics is managed with an instance of root_report class, but is decoupled from the project in case another library wants to be used.

Requirements are automatically checked by downloading via pip the package. The development environment is fully reported in requirements.txt

Project developed by:
* Anderlini L., Graziani G., INFN, Sezione di Firenze;
* Mariani S., INFN e Universita' degli studi di Firenze
* Di Nezza P., INFN, Laboratori Nazionali di Frascati;
* Franzoso E., Pappalardo L., INFN e Universita' degli studi di Ferrara.

and under the terms written under LICENSE.

To avoid conflicts among packages, the environment can be initialized (e.g.) with:
```
   - conda create -n <environment_name> python=3.6.10 tensorflow-gpu==1.15 root -c conda-forge (if a  gpu is available, tensorflow == 1.15 elsewhere )
   - conda activate <environment_name>
   - python setup.py bdist_wheel
   - python -m pip install dist/pid4smog-0.0.0-py3-none-any.whl
```

For users on lxplus with lb-conda available, the conda-create instruction can be replaced by:
```
lb-conda-dev virtual-env default/2020-05-14 <directory name where to save the env>
cd <directory>
./run bash

python --version ==> should print Python 3.7.6
root --version ===> should print ROOT Version: 6.20/04
python -c 'import tensorflow; print(tensorflow.__version__)'   => this should print 1.13. (which is incompatible with the tf.probability we use)

(if not already done) git clone ssh://git@gitlab.cern.ch:7999/samarian/pid4smog_module.git
cd pid4smog_module/
python setup.py bdist_wheel
python -m pip install dist/pid4smog-0.0.0-py3-none-any.whl
pip install -Iv tensorflow==1.15
python -c 'import tensorflow; print(tensorflow.__version__)'   => this should print 1.15.0 now
python config_example.py
```
The results, saved by default in `report_dir`, can be visualized opening in a web browser the .html file (on lxplus, with a ssh tunnel e.g. through sshfs)

Any comment or suggestion can be sent to <a href="mailto:mariani@fi.infn.it"> S. Mariani </a> or an issue can be opened here.
