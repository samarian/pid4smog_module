#########################################################################
#########################################################################
# Project: pid4smog                                                     #
#    File: $Id: utils/make_hist.py                                      #
# @authors = Saverio Mariani mariani@fi.infn.it     1,2                 #
#          = Lucio Anderlini anderlinil@fi.infn.it  2                   #
#                                                                       #
#          1 Universita' degli studi di Firenze                         #
#          2 INFN, Sezione di Firenze                                   #
#  Copyright (c) 2019 - CERN on behalf of the LHCb Collaboration        #
#                                                                       #
#  Redistribution and use in source and binary forms,                   #
#  with or without modification, are permitted according to the terms   #
#  listed in LICENSE                                                    #
#                                                                       #
#########################################################################
#########################################################################
import array as arr
import os

import numpy as np
import root_numpy as rnp
import pandas as pd
import ROOT

from root_report import Report

ROOT.gROOT.SetBatch()
ROOT.gStyle.SetOptStat(0)
ROOT.gStyle.SetPaintTextFormat("2.2f")
ROOT.gErrorIgnoreLevel = ROOT.kWarning

class HistReport (Report):
  """
  class HistReport
  ===============

  Launcher for the root_report library to draw on a html page ROOT graphic objects
  """

  def __init__( self, filename   ,
                path = './www/'  ,
                options = ''     ,
                enabled = True   ,
                var_axes = {},
                lat_message = []):
    """
    #####Class members definition
    arguments:
           filename    : (string) name of the report
           path        : (string) location where to open the html page

           var_axes    : (dict< string, string>) optional helper for the histogram axes drawing
           lat_message : (list<string>) optional common message to be printed on all graphic objects (e.g. the sample)
    """


    if path[:2] == './': path = path[2:]
    if not os.path.exists(path):
      if '/' in path:
        for indx, ipath in enumerate(path.split('/')):
          this_path = ipath if indx == 0 else os.path.join(this_path, ipath)
          if not os.path.exists(this_path): os.mkdir( this_path )

    if not os.path.exists(path + "/img"): os.mkdir("{}/img".format(path))

    super().__init__(filename, path, options = options, enabled = enabled )
    self._var_axes    = var_axes
    self._lat_message = lat_message

  def clear_pad(self):
    "Function to remove all elements drawn on pad"
    for i in range( ROOT.gPad.GetListOfPrimitives().Capacity()):
      ROOT.gPad.GetListOfPrimitives().RemoveLast()

  def get_path( self )      : return self._path
  def get_filename ( self ) : return self._filename
  def get_lat ( self ) : return self._lat_message

  def set_textformat( self, text_fmt = "2.2f" ) :
    " Function to change the ROOT gStyle SetPointTextFormat option to text_fmt (string) "
    ROOT.gStyle.SetPaintTextFormat(text_fmt)

  def draw_canvas( self, name, options = None ) :
    " Function to draw the current ROOT TCanvas with given name (string) and options (string)"

    if  any( isinstance(iprim, ROOT.TH2) for iprim in ROOT.gPad.GetListOfPrimitives() ): right = 0.12
    else : right = 0.05

    self._myCanvas.SetLeftMargin   (0.16)
    self._myCanvas.SetRightMargin  (right)
    self._myCanvas.SetTopMargin    (0.05)
    self._myCanvas.SetBottomMargin (0.20)

    self . outputCanvas ( name.replace(".", "_"), options=options or "width=48%", vector_fmt = "root" )
    self.clear_pad()

  def in_evidence( self, message):
    "Simple Function to print a message in evidence (string) on the html page"
    for i in range(3) : self << "*" * 80 << self.br

    if isinstance(message, str): message = [ message ]
    for im in message:  self <<  im << self.br

    for i in range(3) : self << "*" * 80 << self.br

  def draw_leg( self, name, inputs, x0 = 0.6, y0 = 0.75, x1 = 0.9, y1 = 0.92, outputCanvas = True,
                options = None, leg_opt = 'l' ):
    """
    Function to draw a ROOT TLegend on the current canvas
    arguments:
            name   (string)                          : name of the current canvas
            inputs (dict (string, <TH1, TH2, ....>)) : labels and elements to be added to the TLegend
            x0, y0, x1, y1 (float)                   : TLegend coordinates
            outputCanvas (bool)                      : draw the canvas?
            options (string)                         : canvas drawing option
            leg_opt (string or list<string>)         : TLegend label options
    """

    leg = ROOT.TLegend(x0, y0, x1, y1)
    leg.SetTextFont(132)
    leg.SetLineWidth(0)
    leg.SetFillStyle(0)

    for ih, ilabel in inputs.items():
      if isinstance(ilabel, list ):
        leg.AddEntry(ih, "#scale[1.2]{#bf{%s}}" %ilabel[0], ilabel[1])
      elif isinstance(ilabel, ROOT.TH1) :
        leg.AddEntry(ilabel, "#scale[1.2]{#bf{%s}}" %ih, leg_opt)
      else:
        leg.AddEntry(ih, "#scale[1.2]{#bf{%s}}" %ilabel, leg_opt)

    leg.Draw("SAME")
    if outputCanvas : self.draw_canvas( name = name, options = options)

  def draw_latex( self, name, x = 0.17, y = 0.88, lat = "", outputCanvas = True, options = None, scale = 1.2):
    """
    Function to draw a ROOT TLatex on the current canvas
    arguments:
            name   (string)                          : name of the current canvas
            x, y   (float)                           : TLatex coordinates
            lat (string or list<string>)             : messages to be printed
            outputCanvas (bool)                      : draw the canvas?
            options (string)                         : canvas drawing option
            scale (float)                            : text scale factor
    """

    Tlatex = ROOT.TLatex()
    Tlatex.SetTextFont(132)
    if isinstance(lat, str): Tlatex.DrawLatexNDC(x, y, "#scale[%f]{#bf{%s}}" %(scale, lat))
    if isinstance(lat, list):
        for i, ilat in enumerate(lat): Tlatex.DrawLatexNDC(x, y - i * 0.06, "#scale[%f]{#bf{%s}}" %(scale,ilat))

    if outputCanvas:
      self.draw_canvas( name = name, options = options )

  def draw_hist( self, name, h, goptions="HIST", options=None, outputCanvas = True, lat=None, log = False,
                 color = None, norm = False, more_labels = False):
    """
    Function to draw a ROOT Object on the current canvas
    arguments:
            name   (string)                          : name of the current canvas
            h      (TH1, TH2 ....)                   : object t be drawn
            goptions (string)                        : object drawing option
            options (string)                         : canvas drawing option
            outputCanvas (bool)                      : draw the canvas?
            lat (string or list<string>)             : messages to be printed with draw_latex
            log (bool or list<bool>)                 : axes logarithimc scale
            color (int or list<int>)                : ROOT object colors
            norm (bool)                              : activates DrawNormalized instead of Draw
            more_labels (bool)                       : activates more axes labels
    """

    assert isinstance(h, list) or isinstance(h, ROOT.TH1) or isinstance(h, ROOT.TH2) or isinstance(h, ROOT.TGraph)
    h = [h] if isinstance(h, ROOT.TH1) or isinstance(h, ROOT.TH2) else h

    if color is not None:
      if isinstance(color, int): color = [color] * len(h)
      assert len(color) >= len(h); "Not enough color provided"
      for i in range(len(color)):
        h[i].SetLineColor( color[i] )
        h[i].SetMarkerColor( color[i] )


    if isinstance(log, bool):  log = [log, log, log]
    ROOT.gPad.SetLogx(log[0])
    if len(log) > 1:  ROOT.gPad.SetLogy(log[1])
    if len(log) > 2:  ROOT.gPad.SetLogz(log[2])

    if isinstance(goptions, str)   :
      goptions = [goptions for i in range(len(h))]
    elif isinstance(goptions, list):
      goptions = [goptions[i] for i in range(len(h))]
    else                           :
      goptions = ["HIST" for i in range(len(h))]

    for i, ih in enumerate(h):
      if i!= 0 : goptions[i] += "SAME"

      ih.SetLineWidth(3)
      for iax in [ih.GetXaxis(), ih.GetYaxis(), ih.GetZaxis()] :
        iax.SetLabelFont(132)
        iax.SetTitleFont(132)

      if more_labels:
        ih.GetYaxis().SetMoreLogLabels()
        ih.GetZaxis().SetMoreLogLabels()
        ih.GetZaxis().SetTitleOffset(1.3)

      if isinstance(ih, ROOT.TH2): ROOT.gPad.SetRightMargin(0.18)
      if norm: ih . DrawNormalized ( goptions[i] )
      else   : ih . Draw ( goptions[i] )

      if i == 0 and lat is not None : self.draw_latex(name, lat = lat, outputCanvas = False)

    if outputCanvas: self.draw_canvas( name = name, options = options)

  def ax_helper ( self, ivar ):
    "Mapping of the ax title, if any"
    if isinstance(ivar, list) : return [self.ax_helper(iivar) for iivar in ivar]

    if ivar in self._var_axes : return self._var_axes [ivar]
    else: return ivar

  def lat_sample( self ):
    "Printing of the common lat_message, if any"
    return self._lat_message;


  def make_hist ( self, name, titles, array, nbins = 100, minaxis=None, maxaxis=None, options=None, goptions="HIST",
                  density=False, outputCanvas=True, weights=None, color=ROOT.kBlue, lat = None,
                  fill_mode = "fill", log = False, alpha = 1) :
    """
    Function to build and possibly draw a ROOT Object on the current canvas
    arguments:
            name   (string)                          : name of the current canvas
            titles (string or list<string>)          : histogram axes titles
            array  (pandas or np.ndarray)            : data to be represented
            nbins  (int or list<int>)                : number of histogram bins
            minaxis, maxaxis (float or list<float>)  : ROOT object axes minima and maxima
            options (string)                         : canvas drawing option
            outputCanvas (bool)                      : draw the canvas?
            lat (string or list<string>)             : messages to be printed with draw_latex
            log (bool or list<bool>)                 : axes logarithimc scale
            color (int or list<int>)                 : ROOT object colors
            density (bool)                           : normalize to unity
            more_labels (bool)                       : activates more axes label
            weights  (pandas or np.ndarray)          : weights to be applied to data
            alpha (float)                            : color alpha
            fill_mode ("fill", "array", "empty" )    : fills histogram, fills TGraph, does not fill
    return : ROOT Object (TH1, TH2, TGraph ...)
    """

    for iarr in [array, weights, minaxis, maxaxis] :
      if iarr is not None and isinstance( iarr, pd.DataFrame) : iarr = iarr.values

    if weights is not None:
      assert len(array) == len(weights); "Provided array and weights with different lenghts"

    ########################################################## Histogram building
    if not isinstance(titles, list): titles = [titles]
    if array.ndim == 1:#will build a TH1
      if len(titles) == 1: titles += ["Normalized distribution"] if density else ["Candidates"]

      if isinstance( nbins, int):
        if minaxis is None: 
          if len(np.unique(array)) > 100 : minaxis = np.percentile(array, 1)
          else:                            minaxis = np.min(array)

        if maxaxis is None: 
          if len(np.unique(array)) > 100 : maxaxis = np.percentile(array, 99)
          else:                            maxaxis = np.max(array)

        h = ROOT.TH1D ( name, "", nbins, minaxis - 0.02 * abs(minaxis), maxaxis + 0.02 * abs(maxaxis) )

      elif isinstance( nbins, list) or isinstance( nbins, np.ndarray):
        h = ROOT.TH1D ( name, "", len(nbins) - 1, arr.array('d', nbins) )

    if array.ndim == 2 : #will build a TH2
      if nbins is not None and isinstance ( nbins, int)       : nbins   = [nbins, nbins]
      if minaxis is not None and isinstance ( minaxis, int)   : minaxis = [minaxis, minaxis]
      if maxaxis is not None and isinstance ( maxaxis, int)   : maxaxis = [maxaxis, maxaxis]


      if isinstance( nbins[0], int):
        if minaxis is None: minaxis = array[:,0].min(), array[:, 1].min()
        if maxaxis is None: maxaxis = array[:,0].max(), array[:, 1].max()
        h = ROOT.TH2D ( name, "", nbins[0], minaxis[0], maxaxis[0], nbins[1], minaxis[1], maxaxis[1] )

      elif isinstance( nbins[0], list) or isinstance( nbins[0], np.ndarray):
        h = ROOT.TH2D ( name, "", len(nbins[0]) - 1, arr.array( 'd', nbins[0]), len(nbins[1]) - 1,
                        arr.array('d', nbins[1]) )
        if "TEXT" not in goptions:
          h.SetMarkerStyle(8)
          h.SetMarkerColorAlpha(color, alpha)
        else: h.SetMarkerColor(ROOT.kBlack)

    h.SetTitle( ";#bf{#scale[1.2]{%s}};#bf{#scale[1.2]{%s}}"%(titles[0], titles[1]) )
    if len(titles) > 2 : h.GetZaxis().SetTitle( "#bf{#scale[1.2]{%s}}"%(titles[2]) )

    ########################################################## Histogram filling
    if   fill_mode == "fill" :
      if weights is None or weights.ndim == 1:
        rnp.fill_hist ( h, array, weights )

    elif fill_mode == "array":
      rnp.array2hist ( array[:-1], h, weights )

    if ( h.GetSumw2N() == 0 ) : h.Sumw2 ()
    ########################################################## Histogram drawing options
    h.SetLineWidth(3)
    h.SetLineColorAlpha(color, alpha)

    if goptions == "F"       : h.SetFillColor(color)

    if density and h.GetSumOfWeights() > 0: h.Scale ( 1. / h.GetSumOfWeights() )
    if h.GetSumOfWeights() != 0 :
      self.draw_hist(name, h, options = options, goptions = goptions, outputCanvas = outputCanvas,
                     lat = lat, log = log )
    return h
