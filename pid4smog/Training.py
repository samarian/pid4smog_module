#########################################################################
#########################################################################
# Project: pid4smog                                                     #
#    File: $Id: Training.py                                             #
# @authors = Saverio Mariani mariani@fi.infn.it     1,2                 #
#          = Lucio Anderlini anderlinil@fi.infn.it  2                   #
#                                                                       #
#          1 Universita' degli studi di Firenze                         #
#          2 INFN, Sezione di Firenze                                   #
#  Copyright (c) 2019 - CERN on behalf of the LHCb Collaboration        #
#                                                                       #
#  Redistribution and use in source and binary forms,                   #
#  with or without modification, are permitted according to the terms   #
#  listed in LICENSE                                                    #
#                                                                       #
#########################################################################
#########################################################################
import sys, pickle
from tqdm import trange
from datetime import datetime
from collections import OrderedDict

import numpy as np
import tensorflow as tf
import tensorflow_probability as tfp
from sklearn.preprocessing import MinMaxScaler, QuantileTransformer
from numpy.random import randint

from .NeuralNet import NeuralNet

class Training( ):
    """
    Training
    =========
    Class to reproduce the bidimensional target pdf with the composition of a tunable number of Gaussian distribution.
    The PID dependence on the feature variables is studied by feeding a set of neural network to initialize the
    parameters of all the Gaussian distributions. By iterating over the number of epochs, the loss function, defined
    through the maximum likelihood fit to training data, is minimized as a function of the feature space.

    The procedure can be logically splitted in three steps:
      1. Input target and feature preprocessing with the MinMaxScaler and QuantileTransformer algorithms

      2. Fit of the target distributions with the sum of multinormal:  sum_i ( w * G_i (y, mu, sigma ) ), with all the
         weights, the means and the standard deviations of each of the Gaussians a function of the features.

      3. Minimization of the loss function

    The steps above are executed by the following methods, respectively: # -> TO BE CHANGED
      1.      Preprocessing
      2., 3.  defining the NN and training
    """

    def __init__ ( self, dataset, variables, parameters, flags, model_dir = './MODELS'):
        """
        ### Class members definition

        arguments:
           dataset    : (pandas DataFrame) calibration data
           variables  : (dict <string, list<string> >)  with keys:
                            target     : PID variables to be reproduced by the model
                            features   : variables influencing the PID response
                            weight     : (string) optional, event weights (e.g. sWeights)

           parameters : (dict <string, <int, float, dict> >) with keys :
                            nGaussians : (int)   number of Gaussian distributions to use in the model
                            nNodes     : (int)   number of NN Nodes to study dependence on features
                            nLayers    : (int)   number of hidden layers in the NN
                            nEpochs    : (list)  number of epochs
                            batch_size : (int)   number of entries considered for each iteration
                            max_lr     : (float) starting learning rate (decreased linearly with the epochs)

           flags      : (dict <string, <bool> >) with keys :
                            __SAVE__     : activates variables, scaling algorithms and tensors saving
                            __All_Hist__ : also fill and draw monitoring histogram

           model_dir : base dir where to save the trained model
        """

        self.dataset = dataset

        #Variable definition
        assert all(ivar in variables for ivar in ["target", "features", "weight"]); "Wrong input variables dict"
        self.target, self.features, self.weight  = variables["target" ], variables["features"], variables["weight"]

        print("Target:   ", self.target)
        print("Features: ", self.features)
        print("Weights:  ", self.weight)
        #parameter setting
        self.nGaussians     = parameters["nGaussians" ]
        self.batch          = parameters["batch_size"]
        self.nEpochs        = parameters["nEpochs"]; assert len(self.nEpochs) == 2; "Wrong epoch arguments"
        self.max_lr         = parameters["max_lr"]

        #flags to change behaviour
        self.__SAVE__     = flags["__SAVE__"]
        self.__All_Hist__ = flags["__All_Hist__"]

        #scalers
        self.scalers = OrderedDict()

        #Neural network implementation
        self.NN = NeuralNet(nLayers = parameters["nLayers" ], nNodes = parameters["nNodes" ])
        ################################################################################

        #where to save trained models?
        self.model_dir = model_dir

    def preprocess( self, report ):
        """
        ### Preprocessing phase

        To ease the NN convergence, target and feature variables are preprocessed:
           - MinMaxScaler       : linear transformation mapping the variable range to [0,1]
           - QuantileTransformer: non-linear transformation applying weights to a given distribution to convert it
             to a Gaussian. It spreads the most frequent values and reduces the outliers contribution.

        arguments:
            report: (HistReport instance) drawing plots
        """

        report<<"Preprocessing phase" << report.br
        if self.__All_Hist__:
            report << 'Distributions before PreProcessing' << report.br
            for varName in self.target + self.features:
                h = report.make_hist( "H_DataDistr_" + varName, report.ax_helper(varName),
                                      self.dataset[varName], lat = report.lat_sample())

                if self.weight is not None:
                    h = report.make_hist ( "H_DataDistrWeighted_" + varName, report.ax_helper(varName),
                                           self.dataset[varName], weights = self.dataset[self.weight],
                                           lat = report.lat_sample() + ["weights applied"] )

        for varName in self.target + self.features:
            scaler = MinMaxScaler() if varName in self.target else QuantileTransformer (output_distribution='normal')
            scaler.fit( self.dataset[varName].values.reshape(-1,1) )
            self.dataset ["SCL_%s" % varName] = scaler.transform( self.dataset[varName].values.reshape(-1,1))

            self.scalers["targ_" + varName if varName in self.target else "feat_" + varName] = scaler
            if self.__All_Hist__:
                if varName == self.target[0] :
                    report << report.br << 'Distributions after PreProcessing' << report.br
                report.make_hist( "h_Preprocessed_" + varName, "preprocessed " + report.ax_helper(varName),
                                  self.dataset ["SCL_%s" % varName], lat = report.lat_sample(),
                                  weights = self.dataset[self.weight] if self.weight is not None else None)

        w = [self.weight] if self.weight is not None else []
        return self.dataset [ ["SCL_%s" % v for v in self.target + self.features ] + w ]
    ###########################################################

    def train( self, report, skimd_df):
        """
        ####Training phase: definiton and application to data of the training model.

        -> Initialization: a composition of a tunable number of multinormal distributions
           (tf.distributions.MultiVariateNormalDiag) is defined with randomly initialized parameters:
               . All Gaussian means   are set to the target mean pm  [ -0.5 * std, 0.5 * std]
               . All Gaussian widths  are set to the [0.1, 1] * target std
               . All Gaussian weights are set to [0.,1.]

           To take into account the linear correlation between the target variables, is added:
               . An angle theta randomly set to [0, 2pi]

           With the use_features flag set to 1., the output of the NNs, taking into account the pdf dependence on the
           features variables, is added to all parameters.

        -> The Gaussian Mixture Model is applied to data and a loss function obtained as the -log of the likelihood
           function is defined.

        -> Training of the model is performed in two steps within the same TensorFlow session: firstly, a coarse scan
           of the parameters is found by disabling the NNs, then the dependence on the features variables is explored.

        arguments:
           report: (HistReport instance) tool to draw and save histograms

        """

        print("*"*80)
        print("*"*30 + " Starting TRAINING" + "*"*30)
        print("*"*80)

        target_array  = skimd_df[["SCL_%s" % v for v in self.target ]].values
        feature_array = skimd_df[["SCL_%s" % v for v in self.features ]].values
        if self.weight is not None: weight_array = skimd_df[ self.weight ].values

        x = tf.compat.v1.placeholder ( tf.float64, shape = (None, len(self.target)  ) , name = 'input_x' )
        c = tf.compat.v1.placeholder ( tf.float64, shape = (None, len(self.features)) , name = 'input_c' )
        use_features   = tf.compat.v1.placeholder_with_default( 1., shape = [], name="use_features")
        signal_weights = tf.compat.v1.placeholder_with_default( tf.ones((tf.shape(x)[0]), dtype = tf.float64),
                                                      shape = (None), name = 'sWeights' )
        # Tensors rough initialization
        ##Mean of the Gaussians
        init_mean   = np.mean(target_array, axis=0) + np.random.uniform(-0.5 * np.std(target_array, axis=0),
                                                                              0.5 * np.std(target_array, axis=0),
                                                                             (self.nGaussians, len(self.target)))
        model_mean = self.NN.NN_model( init_mean.shape )
        integrated_mean  = tf.Variable( init_mean, dtype = tf.float64, name = 'int_mean')
        ev_mean = integrated_mean + tf.cast(use_features, tf.float64) * model_mean( c )
        mean    =  tf.identity(ev_mean, name="mean")

        ##StdDev of the Gaussians
        init_stddev = np.array( [ r * np.std(target_array, axis=0) for r in np.random.uniform(0.1, 1.,
                                                                                              self.nGaussians)],
                                dtype = np.float64 )
        model_stddev = self.NN.NN_model( init_stddev.shape )
        integrated_stddev = tf.Variable( init_stddev, dtype = tf.float64, name = 'int_stddev' )
        ev_stddev = integrated_stddev + tf.cast(use_features, tf.float64) * model_stddev( c )
        stddev    =  tf.identity(ev_stddev, name="width")

        ##Correlation angle
        init_theta  = np.random.uniform( 0., 2*np.pi, (self.nGaussians,))
        model_theta = self.NN.NN_model( init_theta.shape )
        integrated_theta = tf.Variable(init_theta, dtype = tf.float64, name = 'int_theta')
        theta = integrated_theta + tf.cast(use_features, tf.float64) * model_theta( c )

        #Relative Gaussian abundances
        init_weight = np.random.uniform(-10, 10, (self.nGaussians,))
        model_weights = self.NN.NN_model( init_weight.shape )
        integrated_weights =  tf.sigmoid( tf.Variable(init_weight, dtype = tf.float64, name = 'int_weigths') )
        ev_weights = integrated_weights + tf.cast(use_features, tf.float64) * model_weights( c )
        weights    =  tf.identity(ev_weights, name="weights")

        ##Multivariate Gaussian model
        Gaussian = tfp.distributions.MultivariateNormalDiag
        g= Gaussian ( loc = mean, scale_diag = stddev + 1e-7 )

        #Rotation tranformation to take into account correlation between variables
        R = tf.concat( [tf.cos(theta), tf.sin(theta), -tf.sin(theta), tf.cos(theta)], axis = 1)
        R = tf.reshape(R, [ -1, 2, 2, self.nGaussians])
        R = tf.transpose(R, perm = [0,3,1,2])
        data = tf.concat( [tf.expand_dims(x,1)] * self.nGaussians, axis = 1)
        rotated_data = tf.einsum( "egi,egij -> egj", data - mean, R ) + mean
        pdf = tf.reduce_sum ( tf.nn.softmax(weights) * g.prob ( rotated_data ), axis=1, name='pdf' )

        ll = -2.*tf.reduce_mean(signal_weights * tf.math.log( tf.nn.relu( pdf ) + 1e-5)) #likelihood
        lr = tf.compat.v1.placeholder ( tf.float32, shape = []) #learningrate
        fitter = tf.compat.v1.train.RMSPropOptimizer(lr).minimize ( ll )

        self.ss = tf.compat.v1.Session()
        self.ss.run ( tf.compat.v1.global_variables_initializer() )

        for iflag in [ 0, 1 ]:
            losses = []

            progressbar = trange(self.nEpochs[iflag] )
            for iEpoch in progressbar:
                try:
                    indices = randint(0, len(self.dataset), self.batch )
                    itarg  = target_array[indices, :]
                    ifeat  = feature_array[indices, :]

                    this_lr = self.max_lr * ( 1.00001 - iEpoch/self.nEpochs[iflag])
                    inputs  = {x: itarg, c: ifeat, lr: this_lr, use_features : iflag }
                    if self.weight is not None:
                        inputs[signal_weights] = weight_array[ indices ]

                    _, loglikelihood = self.ss.run ( ( fitter, ll),  feed_dict = inputs)
                    progressbar.set_description ( "%.4f, %.6f" % (np.min(loglikelihood), this_lr ))
                    if iflag == 1: losses.append ( loglikelihood )

                except KeyboardInterrupt :
                    print("Requested to stop training")
                    iEpoch = self.nEpochs[iflag] -1
                    break

        g = report.make_hist( "Loss", ["Number of epoch", "Loss value [a.u.]"],
                              np.array(losses,dtype=np.float64) - np.min(losses) + 1, color = 1,
                              fill_mode = "array", nbins = np.arange(len(losses)).astype(np.float64),
                              goptions = "HIST PL", lat = report.lat_sample(), options = "width = 99%" )
        report << "Training stopped at loss " << losses[-1] << report.br

        if self.__SAVE__ and iflag == 1:
            export_dir = self.model_dir + "/PID_saved_model_%s_%s/" % ( report.get_filename().split("/")[-1],
                                                                        datetime.now().strftime("%Y-%m-%d-%H-%M") )
            tf.compat.v1.saved_model.simple_save ( self.ss, export_dir,
                                                   inputs  = { 'X': x, 'W': c, "sWeights" : signal_weights},
                                                   outputs = { 'Y': pdf } )

            with open ( export_dir + "/scalers.pkl", 'wb' ) as f : pickle.dump (  self.scalers, f  )
