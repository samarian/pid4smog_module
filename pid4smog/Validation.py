#########################################################################
#########################################################################
# Project: pid4smog                                                     #
#    File: $Id: Training.py                                             #   
# @authors = Saverio Mariani mariani@fi.infn.it     1,2                 # 
#          = Lucio Anderlini anderlinil@fi.infn.it  2                   #  
#                                                                       # 
#          1 Universita' degli studi di Firenze                         # 
#          2 INFN, Sezione di Firenze                                   #      
#  Copyright (c) 2019 - CERN on behalf of the LHCb Collaboration        # 
#                                                                       #       
#  Redistribution and use in source and binary forms,                   #      
#  with or without modification, are permitted according to the terms   #      
#  listed in LICENSE                                                    #  
#                                                                       #
#########################################################################
#########################################################################
import pickle
from collections import OrderedDict
import sys

import ROOT
import numpy as np
import tensorflow as tf
import pandas as pd

from .Histo_2DComparison import Histo_2DComparison

class Validation:
    """
    Validation
    =========

    Validation is a class to validate the model trained on the calibration channels. Feature are divided in bins 
    with similar population and data and prediction are compared in all possible feature pairs. By iterating over all
    feature pairs and bins, the model can be demonstrated to have learnt the PID dependencies. 

    The procedure can be logically split in two steps:
        1. Division in bins for every feature variable with similar statistics;
        2. Comparison for each feature variable pairs and in each bin of data and TensorFlow prediction.

    The steps above are executed by the following methods, respectively:
        1. init_bins
        2. validation
    """

    def __init__(self, dataset, parameters, flags, weights = None ):
        """
        ### Class members definition

        arguments:
           dataset    : (pandas DataFrame) data of the calibration line to be analyzed
           weights    : (string)       : optionally, column weights name (e.g. sWeights)

           parameters : (dict< string, <> > with keys :
                            trainDir   : (str)  directory where to load the training from
                            nBins      : (int)  number of feature bins
                            nSamples   : (int)  number of uniformly generated entries for validation
                            nProj      : (int)  number of bins to perform 2d histogram projection

           flags      : (dict < string, bool> with keys :
                            __All_Hists__     : (bool) draw all histograms
        """
        self.dataset = dataset
        self.weights = weights

        #parameters
        self.trainDir = parameters["trainDir"]
        self.nBins    = parameters["nBins"]
        self.nSamples = parameters["nSamples"]
        self.nProj    = parameters["nProj"]

        self.Histo_2DComparison = Histo_2DComparison()

        #flags to change behaviour
        self.__All_Hist__ = flags["__All_Hist__"]
        ##################################################################################

    def init_bins(self, report, vars_to_validate):
        """
        ### feature splitting according to similar statistics

        arguments:
            - report: (HistReport instance) to draw and save histograms
            - vars_to_validate: (dict<string, list<float>>) feature variables and bins to be considered (if provided bins are None, a binning according to a similar statistics is evaluated)

        returns   :
            - Bin_limits     : (dict <str, list<float>>) bin limits for all features to be validated
        """
        assert isinstance(vars_to_validate, dict), "Validation.py: Please provide me the vars to validate as { ivar: ibins } (ibins = None asks for a binning with similar population )"

        #####feature are binned asking for similar statistics
        Bin_limits = OrderedDict()

        for ifeat, user_bins in vars_to_validate.items():
            if user_bins is None :  Bin_limits[ ifeat ] = list( pd.qcut( self.dataset[ifeat].values, q=self.nBins,
                                                                         labels = False, retbins = True)[1])
            else :  Bin_limits[ ifeat ] = user_bins
            
        print( "Validation bins : ", ifeat, ": ", Bin_limits)
        self.Histo_2DComparison.SetBins( Bin_limits, report )
        return Bin_limits
        ################################################################################

    def data_TF_comp( self, name, report, histos, messages):
        gopt = "BOX"         if isinstance(histos[0], ROOT.TH2 ) else "E1"
        opt  = "width = 90%" if isinstance(histos[0], ROOT.TH2 ) else "width = 48%"

        for ih in histos:
            ih.SetMaximum( 1.5 * ih.GetMaximum() )
            ih.SetMinimum( histos[0].GetMinimum(0.0001) )

        report.draw_hist( name, histos, goptions = gopt, options = opt, outputCanvas = False)
        lat = []
        for im, ilim in messages.items() :
            par_index = report.ax_helper(im).find("[")
            if par_index == -1:
                lat += ["{} #in [{:2.1f}, {:2.1f}]".format(report.ax_helper(im), ilim[0], ilim[1]) ]
            else :
                lat += ["{}#in [{:2.1f}, {:2.1f}] {}".format(report.ax_helper(im)[:par_index],
                                                             ilim[0],ilim[1], report.ax_helper(im)[par_index:])]

        report.draw_latex(name + "_lat", x = 0.48, lat = lat, options = opt, scale = 0.6, outputCanvas = False)
        leg_labels = OrderedDict( { histos[0] : "LHCb 2017 pNe data", histos[1] : "Model prediction"})
        report.draw_leg(name + "_Leg", inputs = leg_labels, options = opt, x0 = 0.16, y0 = 0.83, x1 = 0.45, y1 = 0.93)
        ##############################################################################################

    def do_validation( self, report, feat_bins, density = True):
        """
        ### Validation phase

        Main loop on bins of feature pairs to validate the model. Histograms from data restricted to the considered
        feature ranges are compared with the prediction by TensorFlow. 
        arguments:
            - report               : HistReport instance to draw and save histograms
            - feat_bins           : dict<str, list<float> > bins for each feature to validate
        """

        print("*"*80)
        print("*"*80)
        print("*"*30 + " Starting VALIDATION" + "*"*30)
        print("*"*80)
        print("*"*80)

        ## Model loading
        in_range = lambda iq, imin, imax : "{} > {:2.2f} & {} < {:2.2f}".format( iq, imin, iq, imax)

        report << "Dataset len " << len(self.dataset) <<report.br
        with tf.Session() as ss:
            scalers = pickle.load ( open ( self.trainDir + "/scalers.pkl" , 'rb' ) )
            target    = [ivar.replace("targ_", "") for ivar in scalers if "targ_" in ivar]
            features  = [ivar.replace("feat_", "") for ivar in scalers if "feat_" in ivar]
            
            tf.compat.v1.saved_model.loader.load (ss, ['serve'], self.trainDir)

            try:
                ###############################TARGET is uniformly generated in [0, 1]
                targetspace, scaledtargetspace = [] , []
                for varName in target:
                    targetspace.append ( np.random.uniform(self.dataset[varName].min(),
                                                           self.dataset[varName].max(), (self.nSamples)) )
                    scaledtargetspace.append(scalers["targ_"+varName].transform( targetspace[-1].reshape(-1,1)) [:,0])

                targetspace = np.array ( targetspace ) . T
                scaledtargetspace = np.array ( scaledtargetspace ) . T

                report << report.br << report.br
                if self.__All_Hist__:
                    for it, itarg in enumerate(target):
                        report.make_hist ( itarg, report.ax_helper(itarg), scaledtargetspace[:,it] )

                for ifeat, ibins in feat_bins.items():
                    for imin, imax in zip( ibins[:-1], ibins[1:]):
                        print(ifeat, imin, imax)
                        idf = self.dataset.query( in_range(ifeat, imin, imax) ).copy()

                        for jfeat, jbins in feat_bins.items():
                            if list(feat_bins.keys()).index(jfeat) <= list(feat_bins.keys()).index(ifeat):continue
                            for jmin, jmax in zip( jbins[:-1], jbins[1:]):
                                print(jfeat, jmin, jmax)
                                ijdf = idf.query( in_range(jfeat, jmin, jmax) ).copy()
                                if len(ijdf) < 10 : continue

                                tag = str.join("_", [ifeat, "{:2.1f}_{:2.1f}".format(imin, imax),
                                                     jfeat, "{:2.1f}_{:2.1f}".format(jmin, jmax)] )

                                report << report.br
                                report << "Limiting feature: " << in_range( ifeat, imin, imax) <<report.br
                                report << "Limiting feature: " << in_range( jfeat, jmin, jmax) <<report.br
                                report << "Number of events is now "<< len(ijdf) << report.br

                                if self.__All_Hist__ and ifeat == list(feat_bins.keys())[0]:
                                    for indx, iwtch in enumerate(features) :
                                        report.make_hist (iwtch + "_" + tag, report.ax_helper(iwtch), ijdf[iwtch] )


                                #############################FEATURES
                                featurespace, scaledfeaturespace = [], []
                                indices = np.random.choice( len(ijdf), self.nSamples )
                                for varName in features:
                                    featurespace.append( ijdf[[varName]].values[indices] )
                                    scaledfeaturespace.append( scalers["feat_" + varName].transform(
                                        featurespace[-1].reshape(-1, 1)) [:,0] )

                                scaledfeaturespace = np.array ( scaledfeaturespace ) . T
                                report << report.br << report.br
                                ###########PERFORMING VALIDATION
                                dict_input = {"input_x:0" : scaledtargetspace, "input_c:0":scaledfeaturespace}
                                pdf_ = ss.run( "pdf:0", feed_dict = dict_input)

                                #2D histogram validation
                                h_data = report.make_hist (
                                    "Data_2D_" + tag, report.ax_helper(target), ijdf[target],
                                    weights = ijdf[self.weights] if self.weights is not None else None,
                                    color = ROOT.kRed, nbins = 150, outputCanvas = False, density=density, 
                                    minaxis = np.min(ijdf[target]), maxaxis = np.max(ijdf[target]))

                                h_pred = report.make_hist (
                                    "Prediction_2D_" + tag, report.ax_helper(target), targetspace, density=density,
                                    nbins= 150, weights=pdf_, alpha = 0.5, color = ROOT.kBlue, outputCanvas =False,  
                                    minaxis = np.min(ijdf[target]), maxaxis = np.max(ijdf[target]))

                                
                                lats = OrderedDict({ ifeat : [imin, imax], jfeat : [jmin, jmax]})
                                self.data_TF_comp("Validation_2D_" + tag, report, [h_data, h_pred], lats)

                                comp = self.Histo_2DComparison.Compare(ifeat + "_" + jfeat, [h_data, h_pred],
                                                                       [imin, jmin])
                                report << "2D Histogram comparison stat test: " << comp << report.br

                                ##Overall projections onto the two axes
                                for iax, iproj in {"X" : lambda h : h.ProjectionX(),
                                                   "Y" : lambda h : h.ProjectionY()}.items():
                                    self.data_TF_comp("ValidProj_" + tag + "_" + iax, report,
                                                      [iproj(h_data), iproj(h_pred)], lats )

                                if self.nProj != 1:
                                    for i in range(self.nProj):
                                        bins_to_proj = int(h_data.GetNbinsY()/self.nProj)
                                        h_data_proj = h_data.ProjectionX("Data_Proj" + str(i), 1 + bins_to_proj * i,
                                                                         1 + bins_to_proj * (i+1))
                                        h_pred_proj = h_pred.ProjectionX("Pred_Proj" + str(i), 1+bins_to_proj * i,
                                                                         1 + bins_to_proj * (i+1))

                                        if h_data_proj.GetSumOfWeights() == 0 or h_pred_proj.GetSumOfWeights() == 0 :
                                            continue
                                        for h in [h_data_proj, h_pred_proj] : h.Scale(1./h.Integral("width"))

                                        lat_min = h_data.GetYaxis().GetBinLowEdge(1+bins_to_proj*i)
                                        lat_max = h_data.GetYaxis().GetBinLowEdge(1+bins_to_proj*(i+1))
                                        lats = OrderedDict({ ifeat : [imin, imax], jfeat : [jmin, jmax] ,
                                                             target[1] : [ lat_min, lat_max] } )
                                        self.data_TF_comp("ValidProj_{}".format(i) + "_" + tag + "_" + iax, report,
                                                          [h_data_proj, h_pred_proj], lats )

            except KeyboardInterrupt :
                print("Requested to stop validation")

            self.Histo_2DComparison.DrawHist( report, text_format = "2.4f" )
