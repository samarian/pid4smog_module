#########################################################################
#########################################################################
# Project: pid4smog                                                     #
#    File: $Id: Training.py                                             #
# @authors = Saverio Mariani mariani@fi.infn.it     1,2                 #
#          = Lucio Anderlini anderlinil@fi.infn.it  2                   #
#                                                                       #
#          1 Universita' degli studi di Firenze                         #
#          2 INFN, Sezione di Firenze                                   #
#  Copyright (c) 2019 - CERN on behalf of the LHCb Collaboration        #
#                                                                       #
#  Redistribution and use in source and binary forms,                   #
#  with or without modification, are permitted according to the terms   #
#  listed in LICENSE                                                    #
#                                                                       #
#########################################################################
#########################################################################
"""Example of configuration file to feed the model"""

__Ax_Titles__ = { "pi_DLLppi" : "#pi DLL_{p,#pi}", "pi_DLLKpi" : "#pi DLL_{K,#pi}" ,
                  "pi_P" : "#pi momentum [MeV/c]", "pi_ETA" : "# pi pseudorapidity",
                  "pi_TRACK_CHI2NDOF" : "#pi track fit #chi^{2}/ndf", "nTracks" : "Number of tracks"}
__lat_message__ = ["Randomly generated dataset"]

from Graphic_Helper import HistReport
import root_pandas as rpd
import os, sys

from tensorflow.python.util import deprecation
deprecation._PRINT_DEPRECATION_WARNINGS = False #Just removing some deprecation warnings

phase      = "training" #Do you want to train a new model or check an overtraining or a validation?
report     = HistReport( "report_example_" + phase, path = "./report_dir",
                         var_axes = __Ax_Titles__, lat_message = __lat_message__)

signal_only = str.join("&", ["pi_TRUE_ID == pi_ID", "p_TRUE_ID == p_ID", "K_TRUE_ID == K_ID"])
probe       = "pi" #Which one is the probe particle
dataset     = rpd.read_root("data/TestNtuple_Lc2pKpi_sW.root", where = signal_only ) #Data to be analysed
print(dataset.columns)

flags     = { "__All_Hist__" : True } #Do you want to draw all the histograms?
variables = { "target"   : [ probe + "_DLLppi", probe + "_DLLKpi" ], #What variables do you want to predict?
              "features" : [ probe + "_P", probe + "_ETA",
                             probe + "_TRACK_CHI2NDOF", "nTracks" ], #Which dependencies to explore?
              "weight"   : None } #Are events weighted (e.g. via sPlot)? Choose in [None, '<w_name> (e.g.w_sig)']

if phase.upper() == "TRAINING"          :
    parameters = { "nGaussians"  :  8                ,  #(int)   number of Gaussian distributions to use in the model
                   "nNodes"      :  16               ,  #(int)   number of NN Nodes to study dependence on features
                   "nLayers"     :  2                ,  #(int)   number of hidden layers in the NN
                   "nEpochs"     : [100, 15000]      ,  #(list)  number of epochs for the coarse and fine training
                   "batch_size"  : 20000             ,  #(int)   number of entries considered for each iteration
                   "max_lr"      : 5e-4              }  #(float) starting learning rate

    flags["__SAVE__"] = True #Do you want to save the model?

    from pid4smog import Training
    model = Training( dataset = dataset, variables = variables, parameters = parameters, flags = flags)
    skimd_df = model.preprocess( report ) #go with the variable preprocessing
    model.train( report, skimd_df )       #go with the training

elif phase.upper() == "OVERTRAINING"    :
    parameters  = {
        "trainDir"   : sorted([os.path.join("MODELS/", f) for f in os.listdir("MODELS")],
                              key =lambda x: os.path.getmtime(x))[-1],  #(str)  directory where to load the training from
        "nBins"      : 10                    ,  #(int)  number of features bins
        "nSamples"   : 500000                ,  #(int)  number of uniformly generated entries for validation
        "nGaussians" : 8                     ,  #(int)  number of Gaussian in the model
        "G_pars"     : ["mean"]              }  #(list string) Gaussian parameters to scan (dflt [mean,width,weights] )

    vars_to_validate = [ "pi_P" ] #which variables do you want to consider? (dflt is all features)

    from pid4smog import Overtraining
    overtraining = Overtraining( dataset, parameters, vars_to_validate)
    overtraining.pars_evolution( report ) #go with the check for overtraining

elif phase.upper() == "VALIDATION"      :
    parameters  = {
        "trainDir"   : sorted([os.path.join("MODELS/", f) for f in os.listdir("MODELS")],
                              key =lambda x: os.path.getmtime(x))[-1],  #(str)  directory where to load the training
        "nBins"      : 4                     ,  #(int)  number of features bins
        "nSamples"   : 5000000               ,  #(int)  number of uniformly generated entries for validation
        "nProj"      : 2                     }  #(int)  number of bins to perform 2d histogram projection

    vars_to_validate = { probe + "_P" : None, "nTracks" : None }
    #which variables and with which bins do you want to consider? (dflt binning is obtained with similar population)

    from pid4smog import Validation
    validation = Validation( dataset, parameters, flags, weights = variables["weight"])
    bins = validation.init_bins( report, vars_to_validate) #calculates binning scheme
    validation.do_validation( report, bins )
